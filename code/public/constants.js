//source: http://www.gaminglogy.com/tutorial/draw-image/
// edited by Marina Knabbe
// edited by Milena Hippler

//------------
//System Values
//------------
var STAGE_WIDTH = 1000,
	STAGE_HEIGHT = 450,
	TIME_PER_FRAME = 120, //this equates to 30 fps
	GAME_FONTS = "bold 20px sans-serif";

var PATH_CHAR1 = "img/turtle-sprite-p1.png";
var PATH_CHAR2 = "img/turtle-sprite-p2.png";
var PATH_CHAR3 = "img/turtle-sprite-p3.png";
var PATH_CHAR4 = "img/turtle-sprite-p4.png";
var TROPHY_PATH1 = "trophies/trophy1.png";
var TROPHY_PATH2 = "trophies/trophy2.png";
var TROPHY_PATH3 = "trophies/trophy3.png";
var TROPHY_PATH4 = "trophies/trophy4.png";

var CHAR_WIDTH = 120,
	CHAR_HEIGHT = 62,
	CHAR1_START_X = 10,
	CHAR1_START_Y = 30,
	CHAR2_START_X = 10,
	CHAR2_START_Y = 130,
	CHAR3_START_X = 10,
	CHAR3_START_Y = 230,
	CHAR4_START_X = 10,
	CHAR4_START_Y = 330,
	IMAGE_START_X = 0,
	IMAGE_START_Y = 0,
	SPRITE_WIDTH = 480;

var TEXT_PRELOADING = "Loading ...",
	TEXT_PRELOADING_X = 450,
	TEXT_PRELOADING_Y = 220;
