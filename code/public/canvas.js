//source: http://www.gaminglogy.com/tutorial/draw-image/
// edited by Marina Knabbe
// edited by Milena Maria Hippler

//------------
//System Vars
//------------
var stage = document.getElementById("gameCanvas");
stage.width = STAGE_WIDTH;
stage.height = STAGE_HEIGHT;
var ctx = stage.getContext("2d");
ctx.fillStyle = '#F0F2BF';
ctx.font = GAME_FONTS;

//---------------
//Preloading ...
//---------------
//Preload Art Assets - Sprite Sheets
var charImage1 = new Image();
charImage1.ready = false;
charImage1.onload = setAssetReady;
charImage1.src = PATH_CHAR1;

var charImage2 = new Image();
charImage2.ready = false;
charImage2.onload = setAssetReady;
charImage2.src = PATH_CHAR2;

var charImage3 = new Image();
charImage3.ready = false;
charImage3.onload = setAssetReady;
charImage3.src = PATH_CHAR3;

var charImage4 = new Image();
charImage4.ready = false;
charImage4.onload = setAssetReady;
charImage4.src = PATH_CHAR4;

var trophy1 = new Image();
trophy1.ready = false;
trophy1.onload = setAssetReady;
trophy1.src = TROPHY_PATH1;

var trophy2 = new Image();
trophy2.ready = false;
trophy2.onload = setAssetReady;
trophy2.src = TROPHY_PATH2;

var trophy3 = new Image();
trophy3.ready = false;
trophy3.onload = setAssetReady;
trophy3.src = TROPHY_PATH3;

var trophy4 = new Image();
trophy4.ready = false;
trophy4.onload = setAssetReady;
trophy4.src = TROPHY_PATH4;

function setAssetReady(){
	this.ready = true;
}

//Display Preloading
ctx.fillRect(0, 0, stage.width, stage.height);
ctx.fillStyle = "#000";
ctx.fillText(TEXT_PRELOADING, TEXT_PRELOADING_X, TEXT_PRELOADING_Y);
var preloader = setInterval(preloading, TIME_PER_FRAME);
var gameloop;
var currX = IMAGE_START_X;
var currY = IMAGE_START_Y;
var numOfClients = 0;
var socket = io({reconnection: false});
var speed;
var player1 = -1;
var player2 = -1;
var player3 = -1;
var player4 = -1;
var playerWin1 = -1;
var playerWin2 = -1;
var playerWin3 = -1;
var playerWin4 = -1;
var playerPos = new Array(player1, player2, player3, player4);
var playerWin = new Array(playerWin1, playerWin2, playerWin3, playerWin4);
var winner = 0;
//Asking for number of clients
socket.emit('clients', 0);

//Receiving number of clients
socket.on('clients', function(value){
	//console.log('recived numberOfClients' + value);
	numOfClients = value;
});

//Receiving position values
socket.on('position', function(value){
	for (var i = 0; i < value.length; i++) {
	  playerPos[i] = value[i];
	}
});

//Receiving reset
socket.on('reset', function(url){
	window.location = url;
});

// Receiving winner order
// {player1, player2, player3, player4}
// 1 == 1st; 2 == 2nd; 3 == 3rd; -1 == no Player
socket.on('winner', function(value){
	winner = 1;
	currX = 0;
	currY = 0;
	for (var i = 0; i < value.length; i++) {
	playerWin[i] = value[i];
	}
});

function preloading(){
	if (charImage1.ready && charImage2.ready && charImage3.ready && charImage4.ready){
		clearInterval(preloader);
		gameloop = setInterval(update, TIME_PER_FRAME);
	}
}

//------------
//Game Loop
//------------
function update(){

	//Clear Canvas
	ctx.fillStyle = '#F0F2BF';
	ctx.fillRect(0, 0, stage.width, stage.height);

	//set position
	for (var i = 0; i < playerPos.length; i++) {
		if(i == 0){
			CHAR1_START_X = playerPos[i];
		}else if(i == 1){
			CHAR2_START_X = playerPos[i];
		}else if(i == 2){
			CHAR3_START_X = playerPos[i];
		}else if(i == 3){
			CHAR4_START_X = playerPos[i];
		}
	}

	//Alway at least two players, so no need to check
	//Draw Image1 - green

	ctx.drawImage(charImage1,currX,currY,CHAR_WIDTH,CHAR_HEIGHT,
					CHAR1_START_X,CHAR1_START_Y,CHAR_WIDTH,CHAR_HEIGHT);
	//Draw a line
	ctx.beginPath();
	ctx.moveTo(10,100);
	ctx.lineTo(990,100);
	ctx.strokeStyle="#01DF01"; //line color
	ctx.lineWidth=3;
	ctx.stroke();
	ctx.closePath();

	//Draw Image2 - blue
	ctx.drawImage(charImage2,currX,currY,CHAR_WIDTH,CHAR_HEIGHT,
					CHAR2_START_X,CHAR2_START_Y,CHAR_WIDTH,CHAR_HEIGHT);
	//Draw a line
	ctx.beginPath();
	ctx.moveTo(10,200);
	ctx.lineTo(990,200);
	ctx.strokeStyle="#0000FF"; //line color
	ctx.lineWidth=3;
	ctx.stroke();
	ctx.closePath();

	if(numOfClients >=3){
		//Draw Image3 - red
		ctx.drawImage(charImage3,currX,currY,CHAR_WIDTH,CHAR_HEIGHT,
						CHAR3_START_X,CHAR3_START_Y,CHAR_WIDTH,CHAR_HEIGHT);
		//Draw a line
		ctx.beginPath();
		ctx.moveTo(10,300);
		ctx.lineTo(990,300);
		ctx.strokeStyle="#FF0000"; //line color
		ctx.lineWidth=3;
		ctx.stroke();
		ctx.closePath();
	}

	if(numOfClients == 4){
		//Draw Image4 - lila
		ctx.drawImage(charImage4,currX,currY,CHAR_WIDTH,CHAR_HEIGHT,
						CHAR4_START_X,CHAR4_START_Y,CHAR_WIDTH,CHAR_HEIGHT);
		//Draw a line
		ctx.beginPath();
		ctx.moveTo(10,400);
		ctx.lineTo(990,400);
		ctx.strokeStyle="#A901DB"; //line color
		ctx.lineWidth=3;
		ctx.stroke();
		ctx.closePath();
	}

	if(winner == 1){
		for (var i = 0; i < playerWin.length; i++) {
			currX = 0;
			currY = 0;
			if(i == 0){
			//player1
			CHAR1_START_X = CHAR1_START_X + 15;
			console.log(CHAR1_START_X);
				if(playerWin[i] == 1){
					//player1, trophy1
					ctx.drawImage(trophy1,currX,currY,CHAR_WIDTH,CHAR_HEIGHT,
						CHAR1_START_X,CHAR1_START_Y,CHAR_WIDTH,CHAR_HEIGHT);
				}else if(playerWin[i] == 2){
					//player1, trophy2
					ctx.drawImage(trophy2,currX,currY,CHAR_WIDTH,CHAR_HEIGHT,
						CHAR1_START_X,CHAR1_START_Y,CHAR_WIDTH,CHAR_HEIGHT);
				}else if(playerWin[i] == 3){
					//player1, trophy3
					ctx.drawImage(trophy3,currX,currY,CHAR_WIDTH,CHAR_HEIGHT,
						CHAR1_START_X,CHAR1_START_Y,CHAR_WIDTH,CHAR_HEIGHT);
				}else if(playerWin[i] == 4){
					//player1, trophy4
					ctx.drawImage(trophy4,currX,currY,CHAR_WIDTH,CHAR_HEIGHT,
						CHAR1_START_X,CHAR1_START_Y,CHAR_WIDTH,CHAR_HEIGHT);
				}
			}else if(i == 1){
				//player2
				console.log(CHAR2_START_X);
				CHAR2_START_X = CHAR2_START_X + 15;
				if(playerWin[i] == 1){
					//player2, trophy1
					ctx.drawImage(trophy1,currX,currY,CHAR_WIDTH,CHAR_HEIGHT,
						CHAR2_START_X,CHAR2_START_Y,CHAR_WIDTH,CHAR_HEIGHT);
				}else if(playerWin[i] == 2){
					//player2, trophy2
					ctx.drawImage(trophy2,currX,currY,CHAR_WIDTH,CHAR_HEIGHT,
						CHAR2_START_X,CHAR2_START_Y,CHAR_WIDTH,CHAR_HEIGHT);
				}else if(playerWin[i] == 3){
					//player2, trophy3
					ctx.drawImage(trophy3,currX,currY,CHAR_WIDTH,CHAR_HEIGHT,
						CHAR2_START_X,CHAR2_START_Y,CHAR_WIDTH,CHAR_HEIGHT);
				}else if(playerWin[i] == 4){
					//player2, trophy4
					ctx.drawImage(trophy4,currX,currY,CHAR_WIDTH,CHAR_HEIGHT,
						CHAR2_START_X,CHAR2_START_Y,CHAR_WIDTH,CHAR_HEIGHT);
				}
			}else if(i == 2){
				//player3
				console.log(CHAR3_START_X);
				CHAR3_START_X = CHAR3_START_X + 15;
				if(playerWin[i] == 1){
					//player2, trophy1
					ctx.drawImage(trophy1,currX,currY,CHAR_WIDTH,CHAR_HEIGHT,
						CHAR3_START_X,CHAR3_START_Y,CHAR_WIDTH,CHAR_HEIGHT);
				}else if(playerWin[i] == 2){
					//player2, trophy2
					ctx.drawImage(trophy2,currX,currY,CHAR_WIDTH,CHAR_HEIGHT,
						CHAR3_START_X,CHAR3_START_Y,CHAR_WIDTH,CHAR_HEIGHT);
				}else if(playerWin[i] == 3){
					//player2, trophy3
					ctx.drawImage(trophy3,currX,currY,CHAR_WIDTH,CHAR_HEIGHT,
						CHAR3_START_X,CHAR3_START_Y,CHAR_WIDTH,CHAR_HEIGHT);
				}else if(playerWin[i] == 4){
					//player2, trophy4
					ctx.drawImage(trophy4,currX,currY,CHAR_WIDTH,CHAR_HEIGHT,
						CHAR3_START_X,CHAR3_START_Y,CHAR_WIDTH,CHAR_HEIGHT);
				}
			}else if(i == 3){
				//player4
				console.log(CHAR4_START_X);
				CHAR4_START_X = CHAR4_START_X + 15;
				if(playerWin[i] == 1){
					//player2, trophy1
					ctx.drawImage(trophy1,currX,currY,CHAR_WIDTH,CHAR_HEIGHT,
						CHAR4_START_X,CHAR4_START_Y,CHAR_WIDTH,CHAR_HEIGHT);
				}else if(playerWin[i] == 2){
					//player2, trophy2
					ctx.drawImage(trophy2,currX,currY,CHAR_WIDTH,CHAR_HEIGHT,
						CHAR4_START_X,CHAR4_START_Y,CHAR_WIDTH,CHAR_HEIGHT);
				}else if(playerWin[i] == 3){
					//player2, trophy3
					ctx.drawImage(trophy3,currX,currY,CHAR_WIDTH,CHAR_HEIGHT,
						CHAR4_START_X,CHAR4_START_Y,CHAR_WIDTH,CHAR_HEIGHT);
				}else if(playerWin[i] == 4){
					//player2, trophy4
					ctx.drawImage(trophy4,currX,currY,CHAR_WIDTH,CHAR_HEIGHT,
						CHAR4_START_X,CHAR4_START_Y,CHAR_WIDTH,CHAR_HEIGHT);
				}
			}
		}
	}

	//Next "frame"
	currX += CHAR_WIDTH;
	if (currX >= SPRITE_WIDTH)
		currX = 0;
}
