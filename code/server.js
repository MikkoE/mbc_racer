/**
* Copyright 2016 CADS Modern Browser Communication
* server.js
*
* Author: Mikko Eberhardt
* Email: mikko.eberhardt@haw-hamburg.de
*
* Created on: 17.09.2015
* Version: 0.9
**/

var fs = require('fs');
var express = require('express');
var app = express();
var conf = require('./config.json');
var http = require('http').Server(app);
var io = require('socket.io')(http);

//include datein
eval (fs.readFileSync('src/serverTimer.js', 'utf8'));
eval (fs.readFileSync('src/QrCode.js', 'utf8'));
eval (fs.readFileSync('src/routing.js', 'utf8'));
eval (fs.readFileSync('src/tick.js', 'utf8'));
eval (fs.readFileSync('src/socketIo.js', 'utf8'));
eval (fs.readFileSync('src/sorting.js', 'utf8'));

//var for setting up server or local (0 = server; 1 = localhost)
var server = 1;

// Server ip von Martins rechner
if (server == 1){
  var ip = "141.22.27.231";
}else {
  var ip = "localhost";
}
var port = "11002";
var url = "http://"+ ip +":"+ port;
var clientUrl = "http://"+ ip + ":" +port +"/client";
if (server == 1){
  generateQRCode(clientUrl,6);
}else {
  readQRCodeFile('qr.png');
}
//Socket für den Monitor
var mSocket;

//Socket für die Clients
var cS1, cS2, cS3, cS4;

//Variablen für den Spielerscone (erreichen des maxValue = sieg)
var pS1 = 0, pS2 = 0, pS3 = 0, pS4 = 0;

//Variablen für die Spielerfarben
var pC1 = '#01DF01',
      pC2 = '#0000FF',
      pC3 = '#FF0000',
      pC4 = '#A901DB';

//Variablen für den SpielerValue
var pV1 = -1, pV2 = -1, pV3 = -1, pV4 = -1;

//Variablen für den SpielerLeft (wer noch dabei ist)
var pL1 = 0, pL2 = 0, pL3 = 0, pL4 = 0;

//Variablen für den SpielerKick (wer soll gekickt werden (zu wenig nachtichten))
var pK1 = 0, pK2 = 0, pK3 = 0, pK4 = 0;

//Variablen für die reigenfolge der gewinner
var pW1 = -1, pW2 = -1, pW3 = -1, pW4 = -1;

//liste der Clients
var clientSockets = new Array(cS1, cS2, cS3, cS4);

//liste der Spielergeschwindigkeiten
var playerSpeed = new Array(pS1, pS2, pS3, pS4);

//Liste der Spielerfarben
var playerColor = new Array(pC1, pC2, pC3, pC4);

//Liste der SpielerValues
var playerValue = new Array(pV1, pV2, pV3, pV4);

//Liste der SpielerValues aus dem letzten durchgang
var playerLeft = new Array(pL1, pL2, pL3, pL4);

//Liste der SpielerWerte die zu wenig nachrichten schicken
var playerKick = new Array(pK1, pK2, pK3, pK4);

//gewinner reihenfolge
var winner = new Array(pW1, pW2, pW3, pW4);

//Anzahl Sekunden die verstreichen bis das Spiel startet (ab 2 CLients)
var clientConnect = 10;//sekunden
var fourClients = 5;

//Anzahl der aktuell verbundenen Clients
var clients = 0;

//Anzahl der aktuell verbundenen Sockets
var registeredClient = 0;

//Variable für den Timer der die Ticks generiert
var sysTimerID = null;

//tick ist im idle Zustand der system Tackt
var tick = 1000;

//innerhalb einesWettlaufes der Tick mit dem die Werte geupdatet Werden
var gameTick = 125;

//zeigt an ab wann der Server bereit ist für den Spielstart
var gameReady = 0;

//maxValue als zielwert für die Spieler
var winScore = 880;

//server States
var ServerState = Object.freeze(
    {"StartUp":1, "Connect":2, "Game":3, "End":4}
  );
ServerState = 1;

// Server Spezifikation
//Server port
var server = http.listen(port, function () {
  var host = server.address().address;
  var port = server.address().port;
  console.log('Turtle-Race listening at http://%s:%s', ip, port);
});
app.use(express.static(__dirname + '/public'));

//setup timer for game and signal monitor to call game url
function startGame(){
  // signal monitor to call game url
  socketSignal(mSocket, 'gamestart', url + '/game');
  ServerState = 3;
  console.log('timer ends.....game ist staring');
  resetSysTimer();
  startSysTimer(gameTick);

}

//reset server variables for neu round
function resetServer(){
  //server state startup
  ServerState = 1;

  //deleting all existing Sockets
  mSocket = undefined;
  for (var i = 0; i < clientSockets.length; i++) {
    clientSockets[i] = undefined;
  }

  //resett all variables
  //array löschen A.splice(0,A.length)
  playerSpeed.splice(0, playerSpeed.length);
  //playerValue.splice(0, playerValue.length);
  playerValue = new Array(-1, -1, -1, -1);
  playerLeft.splice(0, playerLeft.length);
  playerKick.splice(0, playerKick.length);
  winner.splice(-1, winner.length);

  //system variables resett
  clients = 0;
  registeredClient = 0;
  resetSysTimer();
  resetTimer();
  gameReady = 0;
}
