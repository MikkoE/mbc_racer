/**
* Copyright 2016 CADS Modern Browser Communication
* tests.js
*
* Author: Mikko Eberhardt
* Email: mikko.eberhardt@haw-hamburg.de
*
* Created on: 02.03.2016
* Version: 0.1
**/

var fs = require('fs');
var express = require('express');
var app = express();

eval (fs.readFileSync('src/sorting.js', 'utf8'));


app.get('/', function (req, res) {
  testSortWinner();
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});


function testSortWinner(){
  var testArray = new Array(100, 100, -1, -1);
  var result = new Array(0, 0, 0, 0);

  console.log(testArray);
  result = sortWinner(testArray).slice();
  console.log(result);

}
