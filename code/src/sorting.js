/**
* Copyright 2016 CADS Modern Browser Communication
* sorting.js
*
* Author: Mikko Eberhardt
* Email: mikko.eberhardt@haw-hamburg.de
*
* Created on: 02.03.2016
* Version: 0.1
**/

//sort the right order for the winner score
function sortWinner(valueArray){
  var array = new Array(-1, -1, -1, -1);
  var sort = valueArray.slice();

  //descending sort of playerValues
  sort.sort(function(a,b){return b-a});
  console.log(sort);
  //sort the right position in the array
  for (var i = 0; i < array.length; i++) {
    if (sort[i] != -1) {
      if (i == 1 && sort[0] == sort[1]){
        array[i] = i + 1;
      }else if (i == 2 && (sort[0] == sort[1] ||
                           sort[1] == sort[2] ||
                           sort[0] == sort[2] )){
        array[i] = i + 1;
      }else if (i == 3 && (sort[0] == sort[1] ||
                           sort[0] == sort[2] ||
                           sort[0] == sort[3] ||
                           sort[1] == sort[2] ||
                           sort[1] == sort[3] ||
                           sort[2] == sort[3] )) {
        array[i] = i + 1;
      }else {
        array[valueArray.indexOf(sort[i])] = i + 1;
      }
    }
  }
  return array;
}
