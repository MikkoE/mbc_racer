/**
* Copyright 2016 CADS Modern Browser Communication
* QrCode.js
*
* Author: Ricky Stevanus Rusli
* Email: rickystevanus.rusli@haw-hamburg.de
*
* Created on: 13.02.2016
* Version: 0.2
**/

var dataURL = "";
function generateQRCode(url,qrSize){
	var qr = require('qr-js');
	dataURL = qr.toDataURL({ mime: 'image/png', value: url, size: qrSize });
}

function readQRCodeFile(imagePath){
	fs.readFile(imagePath, function(err, originalData){
		var base64Image = new Buffer(originalData, 'binary').toString('base64');
		dataURL = 'data:image/png;base64,'+base64Image;
	});
}

function sendQR(socket){
	//console.log(dataURL);
	socketSignal(socket, 'qr', dataURL);
};
