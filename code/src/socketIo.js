/**
* Copyright 2016 CADS Modern Browser Communication
* socketIo.js
*
* Author: Mikko Eberhardt
* Email: mikko.eberhardt@haw-hamburg.de
*
* Created on: 25.02.2016
* Version: 0.1
**/

// new socket connection
io.on('connection', function (socket) {
  socketRegister(socket);
  //console.log('new Connection');
});

//calculate speed values from client
function speed (data, i){
  //console.log('Data: ' + data + ' playerValue: ' + playerValue[i] );
    playerSpeed[i] = data;
    playerKick[i] = 0;
}

//new socket register
function socketRegister(socket){
  if (ServerState == 1){
    mSocket = socket;
    sendQR(socket);
    ServerState = 2;
  }else if (ServerState == 2){
    clientSockets[registeredClient] = socket;
    clientSockets[registeredClient].on('speed', function(data){
      speed(data, clientSockets.indexOf(this));
    });

    console.log(registeredClient);
    //clienten die Farbe zuteilen
    console.log(playerColor[registeredClient]);
    socketSignal(socket, 'color', playerColor[registeredClient]);

    registeredClient++;
  }else if (ServerState == 3){
    mSocket = socket;
    mSocket.on('clients',function(socket){
      console.log('ask for client number' + registeredClient);
      socketSignal(mSocket, 'clients', registeredClient);
    });
    for (var i = 0; i < clients; i++) {
      playerValue[i] = -1;
      playerSpeed[i] = 0;
    }
  };

}

// send signal to parameter -> socket
function socketSignal(socket, msg, data){
  if (socket != undefined){
    socket.emit(msg, data);
    //console.log('Signal: ' + msg);
  }else {
    console.log('No socket defined!');
  };
}
