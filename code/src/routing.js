/**
* Copyright 2016 CADS Modern Browser Communication
* routing.js
*
* Author: Mikko Eberhardt
* Email: mikko.eberhardt@haw-hamburg.de
*
* Created on: 25.02.2016
* Version: 0.2
**/

//var fs = require("fs");

//Monitor aufruf
app.get('/', function(req, res) {
  if (mSocket != undefined )
  if (!mSocket.connected ){
    resetServer();
  }
  if (ServerState == 1){
    console.log('startup ' + ServerState);
    fs.readFile('Monitor/index.html',function (err, data){
      res.writeHead(200, {'Content-Type': 'text/html','Content-Length':data.length});
      res.write(data);
      res.end();
    });
  }else {
    //fehler senden
    fs.readFile('error.html',function (err, data){
      res.writeHead(200, {'Content-Type': 'text/html','Content-Length':data.length});
      res.write(data);
      res.end();
    });
    };
  });

  //Clienten aufruf und registration
  app.get('/client/', function (req, res) {
    if (ServerState == 3){
      fs.readFile('Client/error.html',function (err, data){
        res.writeHead(200, {'Content-Type': 'text/html','Content-Length':data.length});
        res.write(data);
        res.end();
      });
    }
    else{
      console.log(clients);
      if (clients < 4){
        console.log('client registered' + clients);
        //clientpage senden
        fs.readFile('Client/index.html',function (err, data){
          res.writeHead(200, {'Content-Type': 'text/html','Content-Length':data.length});
          res.write(data);
          res.end();
        });

        if (clients == 1){
          startTimer('2 Clients', clientConnect);
        };
        if (clients == 2){
          resetTimer();
          startTimer('3 Clients', clientConnect);
        };
        if (clients == 3){
          resetTimer();
          console.log('4 Clients....game is starting');
          startTimer('4 Clients', fourClients);
        };
        clients = clients + 1;

      }else {
        fs.readFile('Client/error.html',function (err, data){
          res.writeHead(200, {'Content-Type': 'text/html','Content-Length':data.length});
          res.write(data);
          res.end();
        });
      };
    }
  });

  //Game
  app.get('/game/', function (req, res) {
    if (ServerState == 3){
      console.log('game started');
      fs.readFile('game/index.html',function (err, data){
        res.writeHead(200, {'Content-Type': 'text/html','Content-Length':data.length});
        res.write(data);
        res.end();
      });
    }else {
      //fehler senden
      fs.readFile('error.html',function (err, data){
        res.writeHead(200, {'Content-Type': 'text/html','Content-Length':data.length});
        res.write(data);
        res.end();
      });
    };
  });

  //reset server
  app.get('/reset/', function (req, res) {

    console.log('server reset');
    ServerState = 1;
      //variablen zurücksetzen
      resetTimer();
      resetSysTimer();
      resetServer();
  });
