/**
* Copyright 2016 CADS Modern Browser Communication
* tick.js
*
* Author: Mikko Eberhardt
* Email: mikko.eberhardt@haw-hamburg.de
*
* Created on: 25.02.2016
* Version: 0.4
**/

//starte den SystemTick
//startSysTimer(tick);

//SystemTick routine
function sysTick() {

  /**
  * server States
  * 1. startup
  * 2. client register
  * 3. game
  * 4. finish -> reset
  **/
  switch(ServerState){
    case 3: gameLoop();
            console.log(playerValue);
            break;


    case 4: 
            break;

    default:
            //überprüfen ob das game startbereit ist
            gameIsReady();
            //  console.log("SystemTick");
  }
};



//testing game to be ready to start
function gameIsReady(){
  if (gameReady == 1) {
    if (clients < 4 ){
      startGame();
      gameReady = 0;
    }else {
      if (clientSockets[3] != undefined){
        startGame();
        gameReady = 0;
      }
    }
  }
};

function gameLoop(){
  //send clients update Ticks
  for (var i = 0; i < clients; i++) {
    if (clientSockets[i] != undefined){
      socketSignal(clientSockets[i], 'update', 2);
    }
  }

  //check which player wins
  for (var i = 0; i < clients; i++) {
    if (clientSockets[i] != undefined){
      if (playerValue[i] > winScore){
        console.log('playerValues: ' + playerValue);
        winner = sortWinner(playerValue).slice();
        leadTimer('GameFinished', 7);
        socketSignal(mSocket, 'winner', winner);
        console.log('winner is ' + winner);
        //server goes to state End
        ServerState = 4;
        break;
        }
      }
    }


  //send server update Tick
  if (mSocket != undefined){
    socketSignal(mSocket, 'update', 1);

    //send Positionvalues to monitor
    socketSignal(mSocket, 'position', playerValue);
  }
  for (var i = 0; i < playerValue.length; i++) {
    playerValue[i] = playerValue[i] + playerSpeed[i];
  }

  //test for clients to be removed from game
  /*for (var i = 0; i < clients; i++) {
    playerKick[i] += 1;
    if (playerKick[i] > 4){
      clientSockets[i] = undefined;
      console.log('Player '+ i +'were kicked for inactivity');
    }
  }*/
}

//clear sysTimer
function resetSysTimer(){
  clearInterval(sysTimerID);
};
