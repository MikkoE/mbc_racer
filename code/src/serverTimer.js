/**
* Copyright 2016 CADS Modern Browser Communication
* serverTimer.js
*
* Author: Mikko Eberhardt
* Email: mikko.eberhardt@haw-hamburg.de
*
* Created on: 22.09.2015
* Version: 0.6
**/
var timerID;
var tname;
var ti;


//count down function for timer
function cdTimer(name, i) {
  if (i > 0) {
    console.log(name + ': ' + i);
    if (mSocket != undefined) {
      socketSignal(mSocket, 'timer', i);
    }
    i--;
  }else{
    //do shit that should happen if timer ends
    resetTimer();
    if (ServerState == 2){
      startGame();
      console.log('game is starting');
    }else if (ServerState == 4) {
      socketSignal(mSocket, 'reset', url);
      resetServer();
      ServerState = 1;
      console.log('reset server');
    }
  };
  ti = i;
}

//timer before game is starting
function startTimer(name, i){
  if (i == undefined){
    i = 10;
    console.log('default timer = 10s');
  };
  tname = name;
  ti = i;
  console.log('new Timer' + tname);
  timerID = setInterval(time, 1000);

}

//timer while leaderboard is shown before reset
function leadTimer(name, i){
  if (i == undefined){
    i = 10;
    console.log('default timer = 10s');
  };
  tname = name;
  ti = i;
  console.log('new Timer' + tname);
  timerID = setInterval(time, 1000);

}

//funktion to be called in Interval
function time(){
  cdTimer(tname, ti);
}

//systemTick Handle
function startSysTimer(tick){
  sysTimerID = setInterval(sysTick, tick);
};

//clear sysTimer
function resetSysTimer(){
  clearInterval(sysTimerID);
};

//reset timerspecs
function resetTimer(){
  console.log('timer reset');
  clearInterval(timerID);
  timerID = undefined;
}
