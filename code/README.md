# MBC-Race
A small race developed in modern browser communication

Signals from the Server:
  1. Color          : Sends color to client
  2. gamestart      : Sends monitor game url
  3. clients        : Sends number of registered player (only Monitor)
  4. update         : SystemTick
  5. qr				: QR-Code in DataURL Format
  6. position       : send position of players to the monitor
  7. reset          : Sends monitor startUrl for reset
  8. winner         : Sends Monitor winner array
  9. timer         : sends timerValue to monitor

Signal from the MonitorClient:
  1. clients        : Ask server for actual client number

  //fehlt heartbeat

Signals from the ControllerClient:
  1. speed          : send speedValue from client to server  
